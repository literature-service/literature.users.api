services:
  keycloak-db:
    restart: unless-stopped
    image: postgres
    environment:
      POSTGRES_DB: $POSTGRES_DB
      POSTGRES_USER: $POSTGRES_USER
      POSTGRES_PASSWORD: $POSTGRES_PASSWORD
    ports:
      - 5433:5432
    volumes:
      - ./keycloak-db:/var/lib/postgresql/data
    healthcheck:
      test: [ "CMD-SHELL", "pg_isready" ]
      interval: 10s
      timeout: 5s
      retries: 5
  keycloak:
    image: jboss/keycloak
    restart: unless-stopped
    container_name: local_keycloak
    environment:
      DB_VENDOR: postgres
      DB_ADDR: keycloak-db
      DB_DATABASE: $POSTGRES_DB
      DB_USER: $POSTGRES_USER
      DB_PASSWORD: $POSTGRES_PASSWORD
      KEYCLOAK_USER: $KEYCLOAK_USER
      KEYCLOAK_PASSWORD: $KEYCLOAK_PASSWORD
      KEYCLOAK_LOGLEVEL: INFO
    ports:
      - 8080:8080
    depends_on:
      keycloak-db:
        condition: service_healthy
  prometheus:
    image: prom/prometheus
    restart: unless-stopped
    ports:
      - 9090:9090
    volumes:
      - ./prometheus:/prometheus/
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
  grafana:
    image: grafana/grafana
    restart: unless-stopped
    ports:
      - 9000:3000
    volumes:
      - ./grafana:/var/lib/grafana
  jaeger:
    container_name: jaeger
    restart: unless-stopped
    image: jaegertracing/all-in-one
    environment:
      COLLECTOR_OTLP_ENABLED: true
      COLLECTOR_ZIPKIN_HOST_PORT: :9411
    ports:
      - 5775:5775/udp
      - 6831:6831/udp
      - 6832:6832/udp
      - 5778:5778
      - 16686:16686
      - 14250:14250
      - 14268:14268
      - 14269:14269
      - 4317:4317
      - 4318:4318
      - 9411:9411
  users-api:
    image: registry.gitlab.com/literature-service/literature.users.api:latest
    restart: unless-stopped
    ports:
      - 5050:80
      - 5054:84
    environment:
      ASPNETCORE_HTTP_PORT: 80
      ASPNETCORE_DEBUG_PORT: 84
      KeycloakOptions:HostAddress: http://keycloak:8080
      KeycloakOptions:Realm: $KEYCLOAK_REALM
      KeycloakOptions:AdminClientId: $KEYCLOAK_ADMIN_CLIENT_ID
      KeycloakOptions:AdminClientSecret: $KEYCLOAK_ADMIN_CLIENT_SECRET
      KeycloakOptions:AdminGrantType: $KEYCLOAK_ADMIN_GRANT_TYPE
      KeycloakOptions:ClientId: $KEYCLOAK_CLIENT_ID
      JAEGER_SERVICE_NAME: Literature.Users.Api
      JAEGER_AGENT_HOST: jaeger
      JAEGER_AGENT_PORT: 6831
      JAEGER_SAMPLER_TYPE: const
      JAEGER_SAMPLER_PARAM: 1
  rabbitmq:
    image: rabbitmq:3-management-alpine
    ports:
      - 15672:15672
  moderation-db:
    image: postgres:latest
    volumes:
      - ./moderation_db:/var/lib/postgresql/data:rw
    environment:
      - POSTGRES_PASSWORD=$MODERATION_DB_PASSWORD
      - POSTGRES_USER=$MODERATION_DB_USERNAME
      - POSTGRES_DB=$MODERATION_DB_NAME
    healthcheck:
      test: [ "CMD-SHELL", "pg_isready" ]
      interval: 10s
      timeout: 5s
      retries: 5
  moderation-api:
    image: maxbqb/favorite-literature-moderation-api:latest
    environment:
      - DB_HOST=moderation-db
      - DB_NAME=$MODERATION_DB_NAME
      - DB_USERNAME=$MODERATION_DB_USERNAME
      - DB_PASSWORD=$MODERATION_DB_PASSWORD
      - MQTT_HOST=rabbitmq
    depends_on:
      - moderation-db
      - rabbitmq
  works-db:
    image: postgres:latest
    environment:
      POSTGRES_DB: $WORKS_DB_NAME
      POSTGRES_USER: $WORKS_DB_USERNAME
      POSTGRES_PASSWORD: $WORKS_DB_PASSWORD
    volumes:
      - ./works-db:/var/lib/postgresql/data
  works-api:
    image: dedicated407/literature-works-api:latest
    environment:
      "ConnectionStrings:DefaultConnection": $WORKS_DB_OPTIONS
      "RabbitMq:HostName": rabbitmq
    depends_on:
      - rabbitmq
      - works-db
  krakend:
    image: devopsfaith/krakend
    ports:
      - 8000:8080
    volumes:
      - ./krakend.json:/etc/krakend/krakend.json
