﻿using Keycloak.Client;
using Literature.Users.Api.Auth;
using Literature.Users.Api.RestClient.RequestModels;
using Literature.Users.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Literature.Users.Api.Controllers;

[ApiController]
[Route("api/users")]
public class UsersController : ControllerBase
{
    private readonly IKeycloakAuthApi _keycloakAuthApi;
    private readonly IRegistrationService _registrationService;

    public UsersController(IKeycloakAuthApi keycloakAuthApi, IRegistrationService registrationService)
    {
        _keycloakAuthApi = keycloakAuthApi;
        _registrationService = registrationService;
    }

    /// <summary>
    /// Регистрация нового обычного пользователя
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> RegisterCommonUser([FromBody] CreateUserRequestModel request)
    {
        await _registrationService.RegisterUser(request);

        return Ok();
    }

    /// <summary>
    /// Регистрация нового пользователя модератора
    /// </summary>
    [HttpPost("moderators")]
    [Authorize(nameof(Policies.RequireAdmin))]
    public async Task<IActionResult> RegisterModerator([FromBody] CreateUserRequestModel request)
    {
        await _registrationService.RegisterUserWithRole(request, "literature-moderator");

        return Ok();
    }

    /// <summary>
    /// Регистрация нового пользователя писателя
    /// </summary>
    [HttpPost("writers")]
    public async Task<IActionResult> RegisterWriter([FromBody] CreateUserRequestModel request)
    {
        await _registrationService.RegisterUserWithRole(request, "literature-writer");

        return Ok();
    }

    /// <summary>
    /// Регистрация нового пользователя
    /// </summary>
    [Authorize]
    [HttpGet("profile")]
    public async Task<IActionResult> GetProfile()
    {
        var token = Request.Headers["Authorization"];
        var response = await _keycloakAuthApi.GetProfile(token);

        return Ok(response);
    }
}
