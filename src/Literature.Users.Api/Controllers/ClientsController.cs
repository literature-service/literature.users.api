﻿using Keycloak.Client;
using Keycloak.Client.Models;
using Literature.Users.Api.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Literature.Users.Api.Controllers;

[ApiController]
[Route("api/clients")]
[Authorize(nameof(Policies.RequireAdmin))]
public class ClientsController : ControllerBase
{
    private readonly IKeycloakAdminApi _keycloakAdminApi;

    public ClientsController(IKeycloakAdminApi keycloakAdminApi)
    {
        _keycloakAdminApi = keycloakAdminApi;
    }

    [HttpGet]
    public async Task<ActionResult<ShortClientModel[]>> GetClients([FromQuery] string clientId = null, int? max = null)
    {
        var searchEnabled = !string.IsNullOrWhiteSpace(clientId);
        var result = await _keycloakAdminApi.GetClients(clientId, searchEnabled, max);

        return Ok(result);
    }

    [HttpPost]
    public async Task<ActionResult> CreateClient([FromBody] CreateClientRequestModel requestModel)
    {
        await _keycloakAdminApi.CreateClient(requestModel);

        return Ok();
    }

    [HttpDelete("{clientId:guid}")]
    public async Task<ActionResult> DeleteClient([FromRoute] Guid clientId)
    {
        await _keycloakAdminApi.DeleteClient(clientId);

        return Ok();
    }
}
