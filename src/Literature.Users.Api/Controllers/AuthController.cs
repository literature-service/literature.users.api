﻿using AutoMapper;
using Keycloak.Client;
using Keycloak.Client.Models;
using Literature.Users.Api.Options;
using Literature.Users.Api.RestClient.RequestModels;
using Literature.Users.Api.RestClient.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Literature.Users.Api.Controllers;

[ApiController]
[Route("api/auth")]
public class AuthController : ControllerBase
{
    private readonly IKeycloakAuthApi _keycloakAuthApi;
    private readonly IMapper _mapper;
    private readonly KeycloakOptions _options;

    public AuthController(IKeycloakAuthApi keycloakAuthApi, IMapper mapper, IOptions<KeycloakOptions> options)
    {
        _keycloakAuthApi = keycloakAuthApi;
        _mapper = mapper;
        _options = options.Value;
    }

    /// <summary>
    /// Получение токена по пароля
    /// </summary>
    [HttpPost("token-by-password")]
    public async Task<ActionResult<GetTokenResponseModel>> GetTokenByPassword([FromBody] SignInRequestModel request)
    {
        var requestModel = new GetTokenRequestModel
        {
            Username = request.Email,
            Password = request.Password,
            ClientId = _options.ClientId,
            GrantType = "password"
        };
        var response = await _keycloakAuthApi.GetToken(requestModel);

        var result = _mapper.Map<GetTokenResponseModel>(response);
        return Ok(result);
    }
}
