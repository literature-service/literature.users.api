using Hellang.Middleware.ProblemDetails;
using Jaeger;
using Jaeger.Senders;
using Jaeger.Senders.Thrift;
using Keycloak.Client.Extensions;
using Literature.Users.Api.Auth;
using Literature.Users.Api.Extensions;
using Literature.Users.Api.Options;
using Literature.Users.Api.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenTracing;
using OpenTracing.Util;
using RestEase;

namespace Literature.Users.Api;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        var keycloakOptions = _configuration.GetRequiredSection(nameof(KeycloakOptions)).Get<KeycloakOptions>();

        services
            .AddAutoMapper(typeof(Startup).Assembly)
            .AddSwaggerSecurity(keycloakOptions);
        services
            .Configure<KeycloakOptions>(_configuration.GetRequiredSection(nameof(KeycloakOptions)).Bind)
            .AddKeycloakClients<MemoryAdminTokenProvider>(keycloakOptions.HostAddress, keycloakOptions.Realm);

        services.AddSingleton(serviceProvider =>
        {
            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            Configuration.SenderConfiguration.DefaultSenderResolver = new SenderResolver(loggerFactory)
                .RegisterSenderFactory<ThriftSenderFactory>();

            var configuration = Configuration.FromEnv(loggerFactory);

            var tracer = configuration.GetTracer();
            GlobalTracer.Register(tracer);
            return tracer;
        });

        services.AddOpenTracing();

        services.AddProblemDetails(options =>
        {
            options.Map<ApiException>(exception => new ProblemDetails
            {
                Status = (int)exception.StatusCode,
                Title = exception.Content,
                Detail = exception.Message
            });
            options.Map<Exception>(exception => new ProblemDetails
            {
                Status = 500,
                Title = exception.Message,
                Type = exception.GetType().FullName,
                Detail = exception.StackTrace
            });
        });

        services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.Authority = keycloakOptions.BaseAddress;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters.ValidateAudience = false;
            });

        services.AddAuthorization(options => options.AddPolicy(nameof(Policies.RequireAdmin),
            builder => builder.Requirements.Add(
                new AssertionRequirement(context => context.User.HasRealmRole("admin")))));

        services.AddSingleton<IRegistrationService, RegistrationService>();
        services.AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseProblemDetails();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints => endpoints.MapControllers());
    }
}
