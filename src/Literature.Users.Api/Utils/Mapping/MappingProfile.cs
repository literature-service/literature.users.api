﻿using AutoMapper;
using Keycloak.Client.Models;
using Literature.Users.Api.RestClient.ResponseModels;

namespace Literature.Users.Api.Utils.Mapping;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<TokenResponseModel, GetTokenResponseModel>();
    }
}
