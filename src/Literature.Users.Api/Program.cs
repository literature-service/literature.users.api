using Inozpavel.Microservices.Platform;
using Microsoft.IdentityModel.Logging;

namespace Literature.Users.Api;

public class Program
{
    public static void Main(string[] args)
    {
        IdentityModelEventSource.ShowPII = true;
        CreateHostBuilder(args).Build().Run();
    }

    private static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UsePlatform<Startup>()
            .ConfigureAppConfiguration(builder => builder.AddJsonFile("appsettings.local.json", true));
}
