﻿using Keycloak.Client;
using Keycloak.Client.Abstractions;
using Keycloak.Client.Models;
using Literature.Users.Api.Options;
using Microsoft.Extensions.Options;

namespace Literature.Users.Api.Services;

public class MemoryAdminTokenProvider : ITokenProvider
{
    private readonly IKeycloakAuthApi _keycloakAuthApi;
    private readonly KeycloakOptions _options;
    private string _accessToken;

    public MemoryAdminTokenProvider(IKeycloakAuthApi keycloakAuthApi, IOptions<KeycloakOptions> options)
    {
        _keycloakAuthApi = keycloakAuthApi;
        _options = options.Value;
    }

    public async ValueTask<string> GetToken()
    {
        if (!string.IsNullOrWhiteSpace(_accessToken))
        {
            return _accessToken;
        }

        return await GetRefreshedToken();
    }

    public async ValueTask<string> GetRefreshedToken()
    {
        var model = new GetTokenRequestModel
        {
            ClientSecret = _options.AdminClientSecret,
            GrantType = _options.AdminGrantType,
            ClientId = _options.AdminClientId
        };
        
        var response = await _keycloakAuthApi.GetToken(model);
        _accessToken = response.AccessToken;

        return _accessToken;
    }
}
