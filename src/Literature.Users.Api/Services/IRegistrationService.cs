﻿using Literature.Users.Api.RestClient.RequestModels;

namespace Literature.Users.Api.Services;

public interface IRegistrationService
{
    Task RegisterUser(CreateUserRequestModel requestModel);

    Task RegisterUserWithRole(CreateUserRequestModel requestModel, string roleName);
}
