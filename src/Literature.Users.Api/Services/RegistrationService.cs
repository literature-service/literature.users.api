﻿using Keycloak.Client;
using Keycloak.Client.Models;
using Literature.Users.Api.RestClient.RequestModels;

namespace Literature.Users.Api.Services;

public class RegistrationService : IRegistrationService
{
    private readonly IKeycloakAdminApi _keycloakAdminApi;

    public RegistrationService(IKeycloakAdminApi keycloakAdminApi)
    {
        _keycloakAdminApi = keycloakAdminApi;
    }

    public Task RegisterUser(CreateUserRequestModel requestModel)
    {
        var request = new UserRequestModel
        {
            FirstName = requestModel.FirstName,
            LastName = requestModel.LastName,
            Email = requestModel.Email,
            Username = requestModel.Email,
            Enabled = true,
            Credentials = new[]
            {
                new CredentialsModel
                {
                    Type = "password",
                    Value = requestModel.Password,
                    Temporary = false
                }
            }
        };

        return _keycloakAdminApi.RegisterUser(request);
    }

    public async Task RegisterUserWithRole(CreateUserRequestModel requestModel, string roleName)
    {
        var role = await _keycloakAdminApi.GetRoleByName(roleName);

        if (role == null)
        {
            throw new ArgumentException(null, nameof(roleName));
        }

        await RegisterUser(requestModel);

        var users = await _keycloakAdminApi.GetUsersByEmail(requestModel.Email);
        var user = users.FirstOrDefault(x => x.Email == requestModel.Email);

        if (user == null)
        {
            throw new ArgumentException(null, nameof(roleName));
        }

        await _keycloakAdminApi.AssignRealmRole(
            user.Id,
            new[] { new RoleRepresentationModel(role.Id, role.Name) });
    }
}
