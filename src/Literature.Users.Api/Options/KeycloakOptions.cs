﻿namespace Literature.Users.Api.Options;

public class KeycloakOptions
{
    public string HostAddress { get; set; }

    public string Realm { get; set; }

    public string AdminBaseAddress => $"{HostAddress}/auth/admin/realms/{Realm}";

    public string BaseAddress => $"{HostAddress}/auth/realms/{Realm}";

    public string AdminClientSecret { get; set; }

    public string AdminClientId { get; set; }

    public string AdminGrantType { get; set; }

    public string ClientId { get; set; }
}
