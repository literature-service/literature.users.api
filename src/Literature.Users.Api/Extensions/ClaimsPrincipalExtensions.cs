﻿using System.Security.Claims;
using System.Text.Json;

namespace Literature.Users.Api.Extensions;

public static class ClaimsPrincipalExtensions
{
    private const string RolesPropertyName = "realm_access";

    public static bool HasRealmRole(this ClaimsPrincipal claimsPrincipal, string roleName)
    {
        var claimValue = claimsPrincipal.FindFirst(RolesPropertyName)?.Value;

        if (claimValue == null)
        {
            return false;
        }

        try
        {
            var document = JsonDocument.Parse(claimValue);

            return document.RootElement.GetProperty("roles").EnumerateArray().Any(x => x.GetString() == roleName);
        }
        catch (Exception)
        {
            return false;
        }
    }
}
