﻿using Keycloak.Client.Clients.Admin;

namespace Keycloak.Client;

public interface IKeycloakAdminApi :
    IUsersApi,
    IClientsApi,
    IRolesClient
{
}
