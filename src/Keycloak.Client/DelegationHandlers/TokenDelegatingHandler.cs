﻿using System.Net;
using Keycloak.Client.Abstractions;

namespace Keycloak.Client.DelegationHandlers;

public class TokenDelegatingHandler : DelegatingHandler
{
    private const string AuthorizationHeaderName = "Authorization";

    private readonly ITokenProvider _tokenProvider;

    public TokenDelegatingHandler(ITokenProvider tokenProvider)
    {
        _tokenProvider = tokenProvider;
    }

    protected override async Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        await AddToken(request, _tokenProvider.GetToken);
        var result = await base.SendAsync(request, cancellationToken);

        if (result.StatusCode is not (HttpStatusCode.Unauthorized or HttpStatusCode.Forbidden))
        {
            return result;
        }
        await AddToken(request, _tokenProvider.GetRefreshedToken);

        return await base.SendAsync(request, cancellationToken);
    }

    private static async Task AddToken(HttpRequestMessage requestMessage, Func<ValueTask<string>> getTokenFunc)
    {
        var headers = requestMessage.Headers;

        if (headers.Contains(AuthorizationHeaderName))
        {
            headers.Remove(AuthorizationHeaderName);
        }

        var token = await getTokenFunc();
        headers.Add(AuthorizationHeaderName, $"Bearer {token}");
    }
}
