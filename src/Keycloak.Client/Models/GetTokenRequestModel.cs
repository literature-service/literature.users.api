﻿namespace Keycloak.Client.Models;

public class GetTokenRequestModel : Dictionary<string, string>
{
    private const string UsernameKey = "username";
    private const string PasswordKey = "password";
    private const string ClientIdKey = "client_id";
    private const string ClientSecretKey = "client_secret";
    private const string GrantTypeKey = "grant_type";

    public string Username
    {
        get => this[UsernameKey];
        set => this[UsernameKey] = value;
    }

    public string Password
    {
        get => this[PasswordKey];
        set => this[PasswordKey] = value;
    }

    public string ClientId
    {
        get => this[ClientIdKey];
        set => this[ClientIdKey] = value;
    }

    public string ClientSecret
    {
        get => this[ClientSecretKey];
        set => this[ClientSecretKey] = value;
    }

    public string GrantType
    {
        get => this[GrantTypeKey];
        set => this[GrantTypeKey] = value;
    }
}
