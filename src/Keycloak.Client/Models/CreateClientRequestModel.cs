﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class CreateClientRequestModel
{
    [JsonProperty("clientId")]
    public string ClientId { get; set; }

    [JsonProperty("protocol")]
    public string Protocol { get; set; }

    [JsonProperty("enabled")]
    public bool Enabled { get; set; }

    [JsonProperty("bearerOnly")]
    public bool BearerOnly { get; set; }

    [JsonProperty("fullScopeAllowed")]
    public bool FullScopeAllowed { get; set; }

    [JsonProperty("publicClient")]
    public bool PublicClient { get; set; }
}
