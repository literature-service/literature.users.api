﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class RoleRepresentationModel
{
    public RoleRepresentationModel()
    {
    }

    public RoleRepresentationModel(Guid id, string name)
    {
        Id = id;
        Name = name;
    }

    [JsonProperty("id")]
    public Guid Id { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }
}
