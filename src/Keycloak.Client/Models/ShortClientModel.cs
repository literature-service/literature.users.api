﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class ShortClientModel
{
    [JsonProperty("id")]
    public Guid Id { get; set; }

    [JsonProperty("clientId")]
    public string ClientId { get; set; }

    [JsonProperty("enabled")]
    public bool Enabled { get; set; }
}
