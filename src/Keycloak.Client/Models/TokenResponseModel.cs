﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class TokenResponseModel
{
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }

    [JsonProperty("expires_in")]
    public long ExpiresIn { get; set; }

    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }

    [JsonProperty("refresh_expires_in")]
    public long RefreshExpiresIn { get; set; }

    [JsonProperty("token_type")]
    public string TokenType { get; set; }

    [JsonProperty("not-before-policy")]
    public long NotBeforePolicy { get; set; }
    
    [JsonProperty("scope")]
    public string Scope { get; set; }
}
