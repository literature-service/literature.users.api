﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class UserRequestModel
{
    [JsonProperty("email")]
    public string Email { get; set; }

    [JsonProperty("enabled")]
    public bool Enabled { get; set; }

    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    [JsonProperty("lastName")]
    public string LastName { get; set; }

    [JsonProperty("username")]
    public string Username { get; set; }

    [JsonProperty("credentials")]
    public CredentialsModel[] Credentials { get; set; } = Array.Empty<CredentialsModel>();

    [JsonProperty("realmRoles")]
    public string[] RealmRoles { get; set; } = Array.Empty<string>();
}
