﻿using Newtonsoft.Json;

namespace Keycloak.Client.Models;

public class UserShortModel
{
    [JsonProperty("id")]
    public Guid Id { get; set; }

    [JsonProperty("email")]
    public string Email { get; set; }
}
