﻿using Keycloak.Client.Models;
using RestEase;

namespace Keycloak.Client;

public interface IKeycloakAuthApi
{
    [Path("realm")]
    public string Realm { get; set; }

    [Post("/auth/realms/{realm}/protocol/openid-connect/token")]
    [SerializationMethods(Body = BodySerializationMethod.UrlEncoded)]
    Task<TokenResponseModel> GetToken([Body(BodySerializationMethod.UrlEncoded)] GetTokenRequestModel getTokenData);

    [Get("/auth/realms/{realm}/protocol/openid-connect/userinfo")]
    Task<string> GetProfile([Header("Authorization")] string token);
}
