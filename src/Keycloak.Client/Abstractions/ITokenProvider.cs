﻿namespace Keycloak.Client.Abstractions;

public interface ITokenProvider
{
    ValueTask<string> GetToken();

    ValueTask<string> GetRefreshedToken();
}
