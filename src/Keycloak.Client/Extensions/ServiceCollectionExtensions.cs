﻿using Keycloak.Client.Abstractions;
using Keycloak.Client.DelegationHandlers;
using Microsoft.Extensions.DependencyInjection;
using RestEase.HttpClientFactory;

// ReSharper disable UnusedMethodReturnValue.Global

namespace Keycloak.Client.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddKeycloakClients<TTokenProvider>(
        this IServiceCollection services,
        string baseAddress,
        string realm)
        where TTokenProvider : class, ITokenProvider
    {
        services
            .AddTransient<TokenDelegatingHandler>()
            .AddRestEaseClient(baseAddress, new AddRestEaseClientOptions<IKeycloakAdminApi>
            {
                InstanceConfigurer = client => client.Realm = realm
            })
            .AddHttpMessageHandler<TokenDelegatingHandler>();

        services.AddRestEaseClient(baseAddress, new AddRestEaseClientOptions<IKeycloakAuthApi>
        {
            InstanceConfigurer = client => client.Realm = realm
        });

        services.AddSingleton<ITokenProvider, TTokenProvider>();

        return services;
    }
}
