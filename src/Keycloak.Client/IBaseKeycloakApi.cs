﻿using RestEase;

namespace Keycloak.Client;

public interface IBaseKeycloakApi
{
    [Path("realm")]
    public string Realm { get; set; }
}
