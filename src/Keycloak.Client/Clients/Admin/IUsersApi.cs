﻿using Keycloak.Client.Models;
using RestEase;

namespace Keycloak.Client.Clients.Admin;

public interface IUsersApi : IBaseKeycloakApi
{
    [Post("/auth/admin/realms/{realm}/users")]
    Task RegisterUser([Body] UserRequestModel requestModel);

    [Get("/auth/admin/realms/{realm}/users")]
    Task<UserShortModel[]> GetUsersByEmail([Query] string email);

    [Post("/auth/admin/realms/{realm}/users/{userId}/role-mappings/realm")]
    Task AssignRealmRole([Path] Guid userId, [Body] RoleRepresentationModel[] roles);
}
