﻿using Keycloak.Client.Models;
using RestEase;

namespace Keycloak.Client.Clients.Admin;

public interface IRolesClient : IBaseKeycloakApi
{
    [Get("/auth/admin/realms/{realm}/roles/{roleName}")]
    Task<RoleRepresentationModel> GetRoleByName([Path] string roleName);
}
