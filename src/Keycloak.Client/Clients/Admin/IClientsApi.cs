﻿using Keycloak.Client.Models;
using RestEase;

namespace Keycloak.Client.Clients.Admin;

public interface IClientsApi : IBaseKeycloakApi
{
    [Get("/auth/admin/realms/{realm}/clients")]
    Task<ShortClientModel[]> GetClients(
        [Query] string clientId = null,
        [Query] bool search = false,
        [Query] int? max = null);

    [Post("/auth/admin/realms/{realm}/clients")]
    Task CreateClient([Body] CreateClientRequestModel requestModel);

    [Delete("/auth/admin/realms/{realm}/clients/{id}")]
    Task DeleteClient([Path("id")] Guid id);
}
