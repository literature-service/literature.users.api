using Keycloak.Client;
using Keycloak.Client.Models;
using Literature.Users.Api.Options;
using Literature.Users.Api.Services;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace Literature.Users.Api.Tests;

public class TokenProviderTests
{
    [Test]
    public async Task TokenWillBeRefreshedOnFirstCall()
    {
        var adminClientMock = new Mock<IKeycloakAuthApi>();
        adminClientMock.Setup(x => x.GetToken(It.IsAny<GetTokenRequestModel>()))
            .ReturnsAsync(new TokenResponseModel
            {
                AccessToken = Guid.NewGuid().ToString()
            });
        var options = new OptionsManager<KeycloakOptions>(new OptionsFactory<KeycloakOptions>(new[]
        {
            new ConfigureOptions<KeycloakOptions>(_ => { })
        }, ArraySegment<IPostConfigureOptions<KeycloakOptions>>.Empty));
        var tokenProvider = new MemoryAdminTokenProvider(adminClientMock.Object, options);

        var token = await tokenProvider.GetToken();

        adminClientMock.Verify(x => x.GetToken(It.IsAny<GetTokenRequestModel>()), Times.Once);
        Assert.NotNull(token);
    }

    [Test]
    public async Task TokenWillNotBeRefreshedOnSecondCall()
    {
        var adminClientMock = new Mock<IKeycloakAuthApi>();
        adminClientMock.Setup(x => x.GetToken(It.IsAny<GetTokenRequestModel>()))
            .ReturnsAsync(new TokenResponseModel
            {
                AccessToken = Guid.NewGuid().ToString()
            });
        var options = new OptionsManager<KeycloakOptions>(new OptionsFactory<KeycloakOptions>(new[]
        {
            new ConfigureOptions<KeycloakOptions>(_ => { })
        }, ArraySegment<IPostConfigureOptions<KeycloakOptions>>.Empty));
        var tokenProvider = new MemoryAdminTokenProvider(adminClientMock.Object, options);

        var token1 = await tokenProvider.GetToken();
        var token2 = await tokenProvider.GetToken();

        adminClientMock.Verify(x => x.GetToken(It.IsAny<GetTokenRequestModel>()), Times.Once);
        Assert.NotNull(token1);
        Assert.NotNull(token2);
    }
}
