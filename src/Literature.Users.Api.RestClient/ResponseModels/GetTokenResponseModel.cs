﻿namespace Literature.Users.Api.RestClient.ResponseModels;

public class GetTokenResponseModel
{
    public string AccessToken { get; set; }

    public long ExpiresIn { get; set; }

    public string RefreshToken { get; set; }

    public long RefreshExpiresIn { get; set; }

    public string TokenType { get; set; }
}
