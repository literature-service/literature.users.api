﻿namespace Literature.Users.Api.RestClient.RequestModels;

public class SignInRequestModel
{
    public string Email { get; set; }

    public string Password { get; set; }
}
