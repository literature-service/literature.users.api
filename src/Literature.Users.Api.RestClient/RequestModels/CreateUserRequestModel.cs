﻿namespace Literature.Users.Api.RestClient.RequestModels;

public class CreateUserRequestModel
{
    public string Email { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Password { get; set; }
}
